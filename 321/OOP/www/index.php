<?php
    spl_autoload_register(function (string $className){
        require_once __DIR__.'/../src/'.str_replace('\\', '/', $className).'.php';
    });

    $route = $_GET['route'] ?? '';
    $routes = require __DIR__.'/routes.php';
    $isRouteFound = false;
    foreach($routes as $pattern => $controllerAndAction){
        preg_match($pattern, $route, $matches);
        if (!empty($matches)){
           $isRouteFound = true;
            break;
        }
    }
    if (!$isRouteFound) {
         echo 'Страница не найдена';
         return;        
    }
    unset($matches[0]);
    $controllerName = $controllerAndAction[0];
    $actionName = $controllerAndAction[1];
    $controller = new $controllerName();
    $controller->$actionName(...$matches);

    // require '../src/MyProject/Models/Users/User.php';
    // require '../src/MyProject/Models/Articles/Article.php';
    // $author = new MyProject\Models\Users\User('Sasha');
    // $article = new MyProject\Models\Articles\Article('Title', 'Text', $author);
    // var_dump($article);
?>
<style>
    .layout {
    width: 100%;
    max-width: 1024px;
    margin: auto;
    background-color: white;
    border-collapse: collapse;
}

.layout tr td {
    padding: 20px;
    vertical-align: top;
    border: solid 1px gray;
}

.header {
    font-size: 30px;
}

.footer {
    text-align: center;
}

.sidebarHeader {
    font-size: 20px;
}

.sidebar ul {
    padding-left: 20px;
}

a, a:visited {
    color: rgb(201, 121, 0);
}
</style>